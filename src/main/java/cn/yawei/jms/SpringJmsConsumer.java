package cn.yawei.jms;

import javax.annotation.Resource;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;

public class SpringJmsConsumer {
	@Resource
	private JmsTemplate jmsTemplate;

	@Resource(name = "messageDestination")
	private Destination destination;

	public String receiveMessage() throws JMSException {
		TextMessage textMessage = (TextMessage) jmsTemplate.receive(destination);
		return textMessage.getText();
	}
}